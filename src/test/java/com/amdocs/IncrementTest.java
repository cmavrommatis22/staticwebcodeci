package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;


public class IncrementTest{

	@Test
	public void testDecreasecounterZero() throws Exception{
		int k=new Increment().decreasecounter(0);
		assertEquals("Input=0", 0, k);

	}
	@Test
        public void testDecreasecounterOne() throws Exception{
                int j=new  Increment().decreasecounter(1);
                assertEquals("Input=1", 1, j);

	}
	@Test
	public void testDecreasecounterTwo() throws Exception{

		int z=new Increment().decreasecounter(2);
                assertEquals("Input>1", 2, z);

	}


}
