package com.amdocs;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class CalculatorTest{
	@Test
	public void testAdd() throws Exception{
		final int result = new Calculator().add();
	        assertEquals("Add", 9, result);

	}
	@Test
	public void testSub() throws Exception{
		final int subResult=new Calculator().sub();
		assertEquals("Sub",3, subResult);
	}

}
